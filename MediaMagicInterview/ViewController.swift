//
//  ViewController.swift
//  MediaMagicInterview
//
//  Created by Kanchan Phalke on 08/07/20.
//  Copyright © 2020 Aditya Kalamkar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var customCollectionView: UICollectionView!
    
    var authorArray = [String]()
    var idArray = [String]()
    var imgLink = "https://picsum.photos/300/300?image="
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set delegates
        customCollectionView.delegate = self
        customCollectionView.dataSource = self
        
        //Register cells
        self.customCollectionView.register(UINib(nibName: "CustomCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CustomCollectionViewCell")
        
        serverCall()
    }
    
    
    func serverCall() {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let url = URL(string: "https://picsum.photos/list")!
        let task = session.dataTask(with: url) { data, response, error in
            print(response)
            // ensure there is no error for this HTTP response
            guard error == nil else {
                print ("error: \(error!)")
                return
            }
            
            // ensure there is data returned from this HTTP response
            guard let content = data else {
                print("No data")
                return
            }
            
            // serialise the data / NSData object into Dictionary [String : Any]
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: content, options: []) as? [[String:Any]] {
                
                    for i in jsonResult{
                        if let author = i["author"] as? String{
                            self.authorArray.append(author)
                        }
                        if let id = i["id"] {
                            let link = self.imgLink + String(describing: id)
                            //print("ASynchronous\(link)")
                            self.idArray.append(link)
                        }
                    }
                    print("author : \(self.authorArray)")
                    print("image: \(self.idArray)")
                    DispatchQueue.main.async {
                        //Setup grid
                        self.setUpGridView()
                        self.customCollectionView.reloadData()
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
         }
        // execute the HTTP request
        task.resume()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setUpGridView()
        DispatchQueue.main.async {
            self.customCollectionView.reloadData()
        }
    }
    
    func setUpGridView() {
        let flow = customCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        //Cell Constraints
        flow.minimumLineSpacing = 10
        flow.minimumInteritemSpacing = 10
        flow.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        let size = CGSize(width:(customCollectionView!.bounds.width-30)/2, height: 250)
        flow.itemSize = size
        
    }
}

extension ViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.authorArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as! CustomCollectionViewCell
        cell.setData(str: self.authorArray[indexPath.row])
        cell.imageFromUrl(img: self.idArray[indexPath.row])
        return cell
    }
}

extension ViewController : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //Flow Layout Constraints
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (customCollectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size)
    }
}
