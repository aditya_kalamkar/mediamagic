//
//  AppDelegate.swift
//  MediaMagicInterview
//
//  Created by Kanchan Phalke on 08/07/20.
//  Copyright © 2020 Aditya Kalamkar. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window : UIWindow?
    let storyboard = UIStoryboard(name: "Main", bundle: nil)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let ncRoot = UINavigationController(rootViewController: initialViewController)
        self.window?.rootViewController = ncRoot
        return true
    }

}

