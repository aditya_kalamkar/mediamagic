//
//  CustomCollectionViewCell.swift
//  MediaMagicInterview
//
//  Created by Kanchan Phalke on 08/07/20.
//  Copyright © 2020 Aditya Kalamkar. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(str : String){
        self.authorLabel.text = str
    }

    func imageFromUrl(img : String){
         let url = URL(string: img)
        if let imageURLCreated = url{
        let data = try? Data(contentsOf: imageURLCreated)

         if let imageData = data {
            imgView.image = UIImage(data: imageData)
         }
        }
    }
}
